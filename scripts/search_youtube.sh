#!/bin/bash
set -euo pipefail

# Export to make it accessible to exported function
export TMPDIR=tmp

YTPREFIX="https://www.youtube.com/watch?v="
QUERIES=$1
QUERYDELIMITER="&&&&"
# Performance is limited by network capacity and YouTube's response time,
# not CPU
JOBS=${JOBS:-4}
BLOCKSIZE=50

get_playlists() {
  JOBSLOT=$1
  shift
  SEARCH="$*"
  if [[ $DEBUG -eq 1 ]]; then
    QUERY=ytsearch5:"$SEARCH"
  else
    QUERY=ytsearchall:"$SEARCH"
  fi
  echo -n "Searching videos for query: '$SEARCH'..."
  youtube-dl --skip-download --flat-playlist --get-id \
    "$QUERY" >> $TMPDIR/ids.$JOBSLOT
  echo "done"
}
export -f get_playlists

rm -rf $TMPDIR
mkdir $TMPDIR

# Split input file into multiple files everytime `$QUERYDELIMITER` occurs
csplit --digits=2 --quiet --prefix="$(basename $QUERIES)." --suppress-matched \
  --elide-empty-files $QUERIES "/$QUERYDELIMITER/" "{*}"
mv $QUERIES.* $TMPDIR

# Get IDs of all videos that matched the search queries
parallel --jobs $JOBS --eta --linebuffer get_playlists {%} {} :::: \
  $TMPDIR/$QUERIES.*

# Merge output of all jobs, remove duplicates and prepend YouTube URL
cat $TMPDIR/ids.* | sort -u | sed -e "s#^#$YTPREFIX#" > $TMPDIR/links_unique
echo "Found $(cat $TMPDIR/links_unique | wc -l) unique video IDs"

# Download JSON metadata for all links
cat $TMPDIR/links_unique | parallel --pipe --jobs $JOBS --round-robin \
  --linebuffer -N$BLOCKSIZE \
  youtube-dl --batch-file - --skip-download --write-info-json \
  --restrict-filenames --output \''output/%(title)s-%(id)s.%(ext)s'\'

if [[ $DEBUG -ne 1 ]]; then
  rm -rf $TMPDIR
fi
