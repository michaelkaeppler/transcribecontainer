#!/usr/bin/env python3

import time

start_time = time.time()

import argparse
import librosa
import librosa.display
import numpy as np
import shutil
import soundfile as sf

from math import log10, sqrt


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("wavefiles", nargs='+')
    parser.add_argument(
        "--max-duration",
        help="Maximum duration of a single slice in seconds",
        type=int,
        default=600,
    )
    parser.add_argument(
        "--debug",
        help="Give debugging output and visualize waveform",
        action="store_true",
    )
    return parser.parse_args()


def search_onsets(audio_data, sample_rate):
    # Onset detection needs mono signal
    audio_data_mono = librosa.to_mono(audio_data)
    C = np.abs(librosa.cqt(y=audio_data_mono, sr=sample_rate))
    onset_envelope = librosa.onset.onset_strength(
        y=audio_data_mono, sr=sample_rate, S=librosa.amplitude_to_db(C, ref=np.max)
    )
    onset_samples = librosa.onset.onset_detect(
        onset_envelope=onset_envelope, sr=sample_rate, units="samples", backtrack=True
    )
    return onset_samples, onset_envelope


def split_audio_file(wavefile, audio_data, sample_rate, splitpoints_samples):
    slice_sample_begin = 0
    # Add last sample in file
    last_sample_idx = audio_data.shape[0] if audio_data.ndim == 1 else audio_data.shape[1]
    splitpoints_samples = np.append(splitpoints_samples, last_sample_idx)
    idx = 0

    for splitpoint in splitpoints_samples:
        # Transpose to the form that the 'soundfile' library expects
        output_slice = audio_data[..., slice_sample_begin:splitpoint].T
        slice_sample_begin = splitpoint
        fname = f"{wavefile}.{idx:03d}"
        print(f"Writing {fname}...")
        sf.write(fname, output_slice, sample_rate, format="WAV", subtype="PCM_16")
        idx += 1


def process_file(wavefile, max_duration, debug):
    print(f"Loading {wavefile}...")
    audio_data, sample_rate = librosa.load(wavefile, sr=None, mono=False)
    print(f"Sample rate: {sample_rate} Hz")
    if audio_data.ndim == 1:
        # For mono files
        sample_count = audio_data.shape[0]
    else:
        sample_count = audio_data.shape[1]

    duration = librosa.get_duration(y=audio_data, sr=sample_rate)
    print(f"Total duration: {duration:.1f} s")
    if duration < max_duration:
        dest = wavefile+'.000'
        print(
            "File shorter than specified maximum length of"
            f" {max_duration} s, no need for splitting.\n"
            f"Copying to {dest}"
        )
        shutil.copy(wavefile, dest)
        return

    max_dur_samples = librosa.time_to_samples(max_duration, sr=sample_rate)
    lookbehind_samples = max_dur_samples // 5

    splitting_samples = [[0]]
    # Create empty placeholder for onset envelope
    onset_envelope = np.zeros(librosa.samples_to_frames(sample_count))

    while True:
        search_slice_end = splitting_samples[-1][-1] + max_dur_samples
        if search_slice_end > sample_count:
            break
        # We cannot look behind the start of the audio data
        search_slice_begin = max(search_slice_end - lookbehind_samples, 0)
        print(
            f"Searching possible split point between samples"
            f" {search_slice_begin} and {search_slice_end}"
        )
        search_slice = audio_data[..., search_slice_begin:search_slice_end]
        onset_samples, onset_envelope_slice = search_onsets(search_slice, sample_rate)
        search_slice_begin_frame = librosa.samples_to_frames(search_slice_begin)
        search_slice_end_frame = search_slice_begin_frame + onset_envelope_slice.size
        onset_envelope[
            search_slice_begin_frame:search_slice_end_frame
        ] = onset_envelope_slice
        if not onset_samples.size:
            raise SystemExit("Found no possible splitting point, aborting.")

        print(f"Found {len(onset_samples)} onsets")

        splitting_samples.append(list(onset_samples + search_slice_begin))

    del splitting_samples[0]
    # Flatten list
    splitting_samples_flat = [j for sub in splitting_samples for j in sub]
    # Used for debugging
    splitting_times = librosa.samples_to_time(splitting_samples_flat, sr=sample_rate)
    # Take last possible onset, TODO: take onset with maximum strength instead?
    splitpoints_samples = list(map(max, splitting_samples))

    if debug:
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(2, sharex=True)
        librosa.display.waveplot(audio_data, sr=sample_rate, ax=ax[0])
        lims = ax[0].get_ylim()

        ax[0].vlines(
            splitting_times,
            lims[0],
            lims[1],
            color="blue",
            linestyle="--",
            linewidth=1,
            alpha=0.8,
            label="Onsets",
        )
        times = librosa.times_like(onset_envelope, sr=sample_rate)
        ax[1].plot(times, onset_envelope / onset_envelope.max())
        plt.show()

    split_audio_file(wavefile, audio_data, sample_rate, splitpoints_samples)


if __name__ == "__main__":
    args = parse_args()
    for wavefile in args.wavefiles:
        process_file(wavefile, args.max_duration, args.debug)
    print(f"Total execution time: {time.time()-start_time:.2f} seconds")
