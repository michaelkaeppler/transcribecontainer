#!/usr/bin/env python3

import argparse
import os
import shutil

from functools import reduce
from operator import add
from mido import MidiFile, MidiTrack


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs="+", help="MIDI files to merge")
    parser.add_argument(
        "-o",
        "--output",
        help="Output file name",
        type=str,
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    if args.output is None:
        # Use first specified file as blueprint for output filename
        stem = args.files[0].split(".wav")[0]
        args.output = stem + ".wav.midi"

    # Short circuit merging, if there was only one file given
    if len(args.files) == 1:
        print(f"Only one file was given, copying to {args.output}")
        shutil.copy(args.files[0], args.output)
        raise SystemExit

    midi_files = []

    for fname in args.files:
        print(f"Opening {fname}...")
        midi_file = MidiFile(fname)
        assert len(midi_file.tracks) == 2
        midi_files.append(midi_file)

    # Using control track and TICKS_PER_BEAT of the first file
    # for the merged one
    ticks_per_beat = midi_files[0].ticks_per_beat
    midi_output = MidiFile(ticks_per_beat=ticks_per_beat)
    midi_output.tracks.append(midi_files[0].tracks[0])

    # Exclude last message, which is "End of Track"
    # Mido will add this message automatically when writing to a file
    notes_tracks = [midi_file.tracks[1][:-1] for midi_file in midi_files]
    notes_track = reduce(add, notes_tracks)
    # TODO: Filter out useless program change messages?
    midi_output.tracks.append(notes_track)

    print(f"Saving to output file {args.output}...")
    midi_output.save(args.output)
