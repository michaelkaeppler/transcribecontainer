#!/usr/bin/env python3

import time

start_time = time.time()

import argparse
import librosa
import librosa.display
import numpy as np
import os
import soundfile as sf

from math import log10, sqrt


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("wavefile")
    parser.add_argument(
        "--max-duration",
        help="Maximum duration of a single slice in minutes",
        type=int,
        default=10,
    )
    parser.add_argument(
        "--silence-percentile",
        help="Percentile of RMS amplitudes that is assumed to contain silence (in %)",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--silence-headroom",
        help="Additional headroom under silence percentiles (in dB)",
        type=int,
        default=3,
    )
    parser.add_argument(
        "--min-silent-duration",
        help="Minimum duration of silence at cutting points (in milliseconds)",
        type=int,
        default=10,
    )
    parser.add_argument(
        "--debug",
        help="Give debugging output and visualize waveform",
        action="store_true",
    )
    return parser.parse_args()


def amplitude_to_dBFS(amplitude):
    RMS_MAX = sqrt(2) / 2
    return 20 * log10(amplitude / RMS_MAX)


def next_power_of_two(x):
    # https://stackoverflow.com/questions/14267555
    # /find-the-smallest-power-of-2-greater-than-or-equal-to-n-in-python
    return 1 << x.bit_length()


def process_file(args):
    print(f"Loading {args.wavefile}...")
    audio_data, sample_rate = librosa.load(args.wavefile, sr=None, mono=False)
    print(audio_data.shape)
    print(f"Sample rate: {sample_rate} Hz")
    duration = librosa.get_duration(y=audio_data, sr=sample_rate)
    print(f"Total duration: {duration:.1f} s")

    # min_silent_duration converted to samples / 2
    frame_length = next_power_of_two(args.min_silent_duration * sample_rate // 2000)
    hop_length = frame_length // 2
    print(f"Chosen frame length in samples: {frame_length}, hop length: {hop_length}")

    rms_per_frame = librosa.feature.rms(
        y=audio_data, frame_length=frame_length, hop_length=hop_length
    )
    rms_max = np.max(rms_per_frame)
    rms_min = np.min(rms_per_frame)
    print(
        f"Maximum RMS amplitude: {rms_max:.2e}"
        f" ({amplitude_to_dBFS(rms_max):.1f} dBFS)"
    )
    print(
        f"Minimum RMS amplitude: {rms_min:.2e}"
        f" ({amplitude_to_dBFS(rms_min):.1f} dBFS)"
    )

    silence_threshold = np.percentile(rms_per_frame, args.silence_percentile)
    silence_threshold_db = amplitude_to_dBFS(silence_threshold)
    print(
        f"Silence threshold ({args.silence_percentile}%-percentile"
        f" of RMS amplitudes): {silence_threshold:.2e}"
        f" ({silence_threshold_db:.1f} dBFS)"
        f" - additional headroom {args.silence_headroom} dB"
    )

    print("Analyzing...")
    non_silent_sample_intervals = librosa.effects.split(
        audio_data, top_db=args.silence_headroom, ref=silence_threshold
    )
    first_sample_idx, last_sample_idx = 0, audio_data.shape[1] - 1
    silent_sample_intervals = np.insert(
        non_silent_sample_intervals, 0, first_sample_idx
    )
    silent_sample_intervals = np.append(silent_sample_intervals, last_sample_idx)
    silent_time_intervals = librosa.samples_to_time(
        silent_sample_intervals, sr=sample_rate
    ).reshape(-1, 2)

    check_interval = lambda iv: (iv[1] - iv[0]) * 1000 > args.min_silent_duration
    is_interval_suitable = np.apply_along_axis(
        check_interval, axis=1, arr=silent_time_intervals
    )
    suitable_intervals = silent_time_intervals[is_interval_suitable]
    print(
        f"Found {silent_time_intervals.shape[0]} silent intervals,"
        f" {len(suitable_intervals)} of them suitable"
        f" (< {args.min_silent_duration} ms)"
    )

    suitable_intervals_midpoints = np.apply_along_axis(
            lambda iv: iv[0] + (iv[1]-iv[0])/2,
            axis=1,
            arr=suitable_intervals
    )
    last_splitpoint = 0
    splitpoints = [0]
    for maybe_splitpoint in suitable_intervals_midpoints:
        if maybe_splitpoint - splitpoints[-1] > args.max_duration*60:
            splitpoints.append(last_splitpoint)
        last_splitpoint = maybe_splitpoint

    del splitpoints[0]

    print(f"Found {len(splitpoints)} splitpoints")

    if args.debug:
        import matplotlib.pyplot as plt
        import matplotlib.patches as mpatches

        fig, ax = plt.subplots()
        librosa.display.waveplot(audio_data, sr=sample_rate, ax=ax)
        lims = ax.get_ylim()

        for interval in suitable_intervals:
            patch = mpatches.Rectangle(
                (interval[0], lims[0]),
                interval[1] - interval[0],
                lims[1] - lims[0],
                alpha=0.3,
                facecolor="red",
            )
            ax.add_patch(patch)

        ax.vlines(splitpoints, lims[0], lims[1], color='blue', linestyle='--',
                linewidth=1, alpha=0.8, label='Split points')
        ax.legend()
        plt.show()

    last_sample = 0
    splitpoints_samples = librosa.time_to_samples(splitpoints, sr=sample_rate)
    # Add last sample in file
    splitpoints_samples = np.append(splitpoints_samples, audio_data.shape[1])
    idx = 0
    basename = os.path.splitext(args.wavefile)[0]

    for splitpoint in splitpoints_samples:
        output_slice = audio_data[:,last_sample:splitpoint].T
        print(output_slice)
        print(output_slice.shape)
        last_sample = splitpoint
        fname=f"{basename}_{idx:02d}.wav"
        print(f"Writing {fname}...")
        sf.write(fname, output_slice, sample_rate, subtype='PCM_16')
        idx += 1



if __name__ == "__main__":
    args = parse_args()
    process_file(args)
    print(f"Total execution time: {time.time()-start_time:.2f} seconds")
