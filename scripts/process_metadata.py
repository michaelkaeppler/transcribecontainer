#!/usr/bin/env python3

from argparse import ArgumentParser
import json
import glob
from multiprocessing import Pool
from tqdm import tqdm
import os
import sys

from process_metadata_filters import CONSTRAINTS


def parse_args():
    args_parser = ArgumentParser()
    args_parser.add_argument(
        "--output", "-o", help="Save video IDs in this file", required=True, type=str
    )
    args_parser.add_argument(
        "--input", "-i", help="Get JSON files from this folder (default: cwd)", type=str
    )
    args_parser.add_argument(
        "--recursive",
        "-r",
        help="Search for JSON files recursively",
        action="store_true",
    )
    args_parser.add_argument(
        "--file-limit",
        "-l",
        help="Process only this amount of files",
        type=int
    )
    return args_parser.parse_args()


def get_json_filepaths(load_path, recursive):
    JSON_EXT = "*.info.json"
    if recursive:
        path = os.path.join(load_path, "**/", JSON_EXT)
    else:
        path = os.path.join(load_path, JSON_EXT)

    return glob.glob(path, recursive=recursive)


def _load_data_worker(json_filepath):
    with open(json_filepath, "r") as json_file:
        try:
            data = json.load(json_file)
        except json.JSONDecodeError:
            print(f"Invalid json file {json_filepath}, skipping")
            data = None

    # Remove information about available formats, it is not needed
    # for filtering and consumes a lot of memory
    del data["formats"]
    return data


def load_data(json_filepaths):
    with Pool() as p:
        all_data = [
            data
            for data in tqdm(
                p.imap_unordered(_load_data_worker, json_filepaths),
                total=len(json_filepaths),
            )
            if data is not None
        ]

    return all_data


def filter_videos(all_data):
    filtered_videos = []
    stats = {
        "total_video_count": len(all_data),
        "duration_all": 0,
        "duration_filtered": 0,
    }

    for video in all_data:
        stats["duration_all"] += video["duration"]
        error = None
        for check in CONSTRAINTS:
            error = check(video)
            if error:
                # Skip further checks if one failed
                break

        if error:
            raw_msg = error["msg"]
            try:
                msg = raw_msg + f" --- like ID {error['duplicate']['id']}"
            except KeyError:
                msg = raw_msg

            print(f"Removing video with ID {video['id']}: {msg}")
            try:
                stats[raw_msg] += 1
            except KeyError:
                stats[raw_msg] = 1
            continue

        filtered_videos.append(video)
        stats["duration_filtered"] += video["duration"]

    stats["filtered_video_count"] = len(filtered_videos)

    return filtered_videos, stats


def show_stats(stats):
    unfiltered_video_count = stats.pop("total_video_count")
    filtered_video_count = stats.pop("filtered_video_count")
    error_count = unfiltered_video_count - filtered_video_count
    duration_all = stats.pop("duration_all") / 3600
    duration_filtered = stats.pop("duration_filtered") / 3600
    for cause, count in stats.items():
        print(f"{cause}: {count} ({count/unfiltered_video_count:.1%})")
    print(f"Total: {error_count}" f" ({error_count/unfiltered_video_count:.1%})")
    print(
        "Duration (all/filtered):" f" {duration_all:.1f} h / {duration_filtered:.1f} h"
    )


def save_ids(path, filtered_data):
    filtered_ids = [json_dict["id"] for json_dict in filtered_data]
    URL_PREFIX = "https://www.youtube.com/watch?v="
    with open(path, "w") as urlfile:
        urlfile.writelines(
            (f"{URL_PREFIX}{filtered_id}\n" for filtered_id in filtered_ids)
        )


if __name__ == "__main__":
    args = parse_args()
    search_path = args.input or os.getcwd()
    print("Loading data...")
    json_filepaths = get_json_filepaths(search_path, args.recursive)
    if args.file_limit and len(json_filepaths) > args.file_limit:
        json_filepaths = json_filepaths[:args.file_limit]

    all_data = load_data(json_filepaths)
    print("Filtering data...")
    filtered_data, stats = filter_videos(all_data)
    print("Removed videos for following reasons:")
    show_stats(stats)
    print(f"Writing IDs to '{args.output}'...")
    save_ids(args.output, filtered_data)
