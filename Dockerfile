FROM continuumio/miniconda3

RUN apt-get update \
    && apt-get --no-install-recommends install -y \
        build-essential \
        ffmpeg \
        libasound2-dev \
        libjack-dev \
        libsndfile1 \
        parallel \
        portaudio19-dev \
        unzip \
        vim

ENV MAGENTA_ENV_NAME=magenta
RUN conda create -n ${MAGENTA_ENV_NAME} python=3.8 \
    && conda run --no-capture-output -n ${MAGENTA_ENV_NAME} \
         pip install magenta youtube-dl

ENV MAGENTA_BASE_DIR=/magenta
ENV MAGENTA_SCRIPTS_DIR=$MAGENTA_BASE_DIR/scripts
ENV MAGENTA_WORKBENCH_DIR=${MAGENTA_BASE_DIR}/workbench
RUN mkdir -p "$MAGENTA_WORKBENCH_DIR"
WORKDIR $MAGENTA_BASE_DIR

RUN wget -nv \
    https://storage.googleapis.com/magentadata/models/onsets_frames_transcription/maestro_checkpoint.zip \
    && unzip maestro_checkpoint.zip \
    && rm maestro_checkpoint.zip

COPY scripts/download_transcribe.sh $MAGENTA_SCRIPTS_DIR/
COPY scripts/split_audio.py $MAGENTA_SCRIPTS_DIR/
COPY scripts/merge_midis.py $MAGENTA_SCRIPTS_DIR/
RUN chmod +x $MAGENTA_SCRIPTS_DIR/download_transcribe.sh

COPY examples $MAGENTA_BASE_DIR/examples

ENTRYPOINT ["/bin/bash"]
