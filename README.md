# TranscribeContainer
Dockerized environment to download audio files and transform them into MIDI files, based on the [Onsets and Frames](https://github.com/magenta/magenta/tree/main/magenta/models/onsets_frames_transcription) model from the [Magenta project](https://github.com/magenta/magenta/blob/main/README.md) and [youtube-dl](https://github.com/ytdl-org/youtube-dl).

## Setup (Dockerized version, recommended)
1. Install [Docker](https://docs.docker.com/engine/install/ubuntu/#set-up-the-repository)
2. Clone the repo, set up workbench
```bash
git clone https://gitlab.com/academai/transcribecontainer.git
mkdir workbench
```
3. Build the image
```bash
cd transcribecontainer
docker build . -t transcribecontainer
```
4. Run container
```bash
docker run -it -v workbench:/magenta/workbench transcribecontainer
```
This command mounts the host directory `workbench` as `/magenta/workbench`
inside the container, which allows sharing input and output data. Note that
all temporary data like downloaded audio files is written to that path
The path `/magenta/workbench` is currently hardcoded.

5. Transcribe example (commands executed inside the running container)
```
cd magenta
scripts/download_transcribe.sh examples/bach_invention_urls
exit
```
6. Find created MIDIs (commands executed on the host again)
```bash
ls workbench/midi/*
```
## Setup (without Docker, not recommended and untested)
1. Install prerequisites (assuming a Debian-like distribution)
```bash
apt-get update
apt-get --no-install-recommends install -y \
  build-essential ffmpeg libasound2-dev libjack-dev libsndfile1 \       
  parallel portaudio19-dev
```
2. Install [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)
3. Create environment and install packages
```bash
conda create -n magenta python=3.8
conda activate magenta
pip install magenta youtube-dl
```
4. Clone the repo, set up workbench
```bash
git clone https://gitlab.com/academai/transcribecontainer.git
mkdir -p magenta/workbench
```
5. Download and unzip model checkpoint
```bash
wget -nv https://storage.googleapis.com/magentadata/models/onsets_frames_transcription/maestro_checkpoint.zip
unzip -d magenta maestro_checkpoint.zip
rm maestro_checkpoint.zip
```
6. Set variables
```bash
export BASE_DIR=magenta
export SCRIPTS_DIR=transcribecontainer/scripts
export WORKBENCH_DIR=magenta/workbench
```
7. Transcribe example
```
cd transcribecontainer
scripts/download_transcribe.sh examples/bach_invention_urls
```

## Operation
### Input
Input to the transcription script is a list of YouTube urls, separated by newlines.
Example:
```
https://www.youtube.com/watch?v=OmoNgAEUyYc
https://www.youtube.com/watch?v=ypv1SSNNUxA
```
The videos mentioned here are licensed under "Creative Commons".
### Download
The videos from the url list are downloaded with `youtube-dl` and the audio
data is extracted. The number of parallel download jobs can be configured
by setting the environment variable `JOBSDOWNLOAD`.
### Preprocessing
In order to stay below the available amount of memory during transcription,
the resulting wave files are then split into smaller chunks. This is done
by looking at possible note onsets and cutting there.
The maximum possible transcription length without running out of memory
when transcribing on CPUs is roughly estimated by the script.
However, it can be overwritten by setting
`MAXIMUM_TRANSCRIPTION_LENGTH` (in seconds).
### Transcription
The audio files are passed in batches to the transcription script
`onsets_frames_transcription_transcribe` to reduce the effect of startup time.
### Postprocessing
All MIDI slices belonging to a video are merged afterwards to a single
MIDI file.
### Logfiles
Three types of logfiles are created in `workbench/log` during operation:
- `download-<date and time>.log`: Output of commands during download
- `transcription-<date and time>.log`: Output of commands during transcription
- `videos-<date and time>.log`: contains a list of video ids with their
transcription state like:

```
OmoNgAEUyYc -- download OK -- transcription OK
fZDOEICxo6k -- download OK -- transcription failed
ypv1SSNNUxA -- download failed
```

## Configurable environment variables
- `MAGENTA_BASE_DIR`: path to a folder that contains subdirectories with
the transcription and helper scripts and to the workbench directory
- `MAGENTA_SCRIPTS_DIR`: path to the scripts if not in
`$MAGENTA_BASE_DIR/scripts`
- `MAGENTA_WORKBENCH_DIR`: path to the workbench directory if not in
`$MAGENTA_BASE_DIR/workbench`
- `JOBSDOWNLOAD`: number of parallel download jobs (defaults to `4`)
- `JOBSTRANSCRIBE`: number of parallel transcription jobs (defaults to `1`)
- `BATCHSIZE`: number of files that are passed to `onsets_frames_transcription_transcribe` in one batch (defaults to `3`)
- `KEEP_WAVES`: set to `1` to keep downloaded audio data in `$WORKBENCH_DIR/wav` after
transcription
- `MAXIMUM_TRANSCRIPTION_LENGTH`: maximum audio duration in seconds to be processed
without splititng

## Further scripts (currently undocumented)
- `search_youtube.sh`: search YouTube by building search queries from
combinations of previously defined parts
- `process_metadata.py`: filter YouTube videos according to specific criteria,
e.g. duration, keywords
- `split_audio.py`: split audio files at note onsets (_used by `download_transcribe.sh`_)
- `split_at_silence.py`: split audio files at silent parts
- `merge_midis.py`: concatenate midi files


