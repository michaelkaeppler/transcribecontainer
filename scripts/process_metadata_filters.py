import re


class SingleVideoCheck:
    def __init__(self, check_func, error_str):
        self.check_func = check_func
        self.error = {"msg": error_str}

    def __call__(self, json_dict):
        try:
            if not self.check_func(json_dict):
                return self.error
        except KeyError as e:
            self.error["msg"] = f"key {e.args[0]} is missing"
            return self.error

        return None


class EqualValueCheck:
    def __init__(self, key, error_str, condition=None):
        self.key = key
        self.unique_values = {}
        self.error = {"msg": error_str}
        # Callable that takes two dicts and returns 'True' if the dicts
        # are counted as duplicates. Possible Use case: remove videos
        # with the same duration, if and only if the composer is identical.
        self.condition = condition

    def __call__(self, json_dict):
        value = json_dict[self.key]
        try:
            json_dict_already_there = self.unique_values[value]
        except KeyError:
            # Save first object for value stored under 'self.key'
            self.unique_values[value] = json_dict
            return None

        if self.condition is None or self.condition(json_dict, json_dict_already_there):
            self.error["duplicate"] = json_dict_already_there
            return self.error


############### Customize this section ###############
######################################################

# Duration in seconds
length = SingleVideoCheck(
    lambda json_dict: 30 < json_dict["duration"] < 7200, "Duration exceeds range"
)

tutorial = SingleVideoCheck(
    lambda json_dict: re.search("tutorial", json_dict["description"]) is None,
    "Word 'tutorial' in description",
)

same_id = EqualValueCheck("id", "duplicate video ID")

CONSTRAINTS = [same_id, length, tutorial]

