#!/bin/bash --login

# Fail instantly on errors ("Bash strict mode")
set -euo pipefail

# Input
URLFILE=$1

## Directories
BASE_DIR=${MAGENTA_BASE_DIR:-~/magenta}
SCRIPTS_DIR=${MAGENTA_SCRIPTS_DIR:-${BASE_DIR}/scripts}
WORKBENCH_DIR=${MAGENTA_WORKBENCH_DIR:-${BASE_DIR}/workbench}

# Export to use them from within exported functions
export MODEL_DIR="$BASE_DIR/train"
export WAVE_DIR="$WORKBENCH_DIR/wav"
export MIDI_DIR="$WORKBENCH_DIR/midi"
export WAVE_TMP_DIR="$WORKBENCH_DIR/temp"
LOG_DIR="$WORKBENCH_DIR/log"

NOW=$(date +"%d%m_%H%M%S")
export LOG_FILE_DOWNLOAD="$LOG_DIR/download-$NOW.log"
export LOG_FILE_TRANSCRIPT="$LOG_DIR/transcription-$NOW.log"
export LOG_FILE_IDS="$LOG_DIR/videos-$NOW.log"

# Absolute paths to Python scripts
export SPLIT_AUDIO="$SCRIPTS_DIR/split_audio.py"
export MERGE_MIDIS="$SCRIPTS_DIR/merge_midis.py"

## Settings
# NUmber of parallel jobs for downloading and transcribing
JOBSDOWNLOAD=${JOBSDOWNLOAD:-4}
JOBSTRANSCRIBE=${JOBSTRANSCRIBE:-1}
# Batch size for transcribing
BATCHSIZE=${BATCHSIZE:-3}

# Do not keep wave files by default
export KEEP_WAVES=${KEEP_WAVES:-0}

rm -rf $WAVE_TMP_DIR
mkdir -p "$WAVE_DIR" "$MIDI_DIR" "$WAVE_TMP_DIR" "$LOG_DIR"

# Disabling bash's strict mode can be required for activation to work
set +euo pipefail
if [ -n "$MAGENTA_ENV_NAME" ]; then
  conda activate $MAGENTA_ENV_NAME
fi
set -euo pipefail

export QUEUEFILE=$WORKBENCH_DIR/queue
rm -f $QUEUEFILE

tstamp() {
  date "+[%T] "
}

export -f tstamp

download_video() {
  URL=$1
  COUNT=$2
  # Remove everything up to the video ID and remove CR characters
  # that may occur if the URL file was generated in Windows
  ID=$(echo $URL | sed 's/^.*watch?v=//' | sed 's/\r$//')
  if youtube-dl --restrict-filenames --extract-audio --audio-format wav \
    --output "${WAVE_TMP_DIR}/%(title)s-%(id)s.%(ext)s" $URL 1>&2; then
    flock $QUEUEFILE printf '%s\n' "$ID" >> $QUEUEFILE
    flock $LOG_FILE_IDS printf '%s -- download OK --\n' "$ID" >> $LOG_FILE_IDS
    echo "$(tstamp)Successfully downloaded video $ID ($COUNT/$VIDEOCOUNT)"
  else
    flock $LOG_FILE_IDS printf '%s -- download failed\n' "$ID" >> $LOG_FILE_IDS
    echo "$(tstamp)Download of video $ID failed"
    return 1
  fi
}

export -f download_video

transcribe_audio() {
  JOBCOUNT=$1
  IDS="${@:2}"
  AUDIOFILES=()
  for ID in $IDS; do
    AUDIOFILES+=($WAVE_TMP_DIR/*-$ID.wav)
  done

  # Redirect stdout to stderr for all following commands
  # Save stdout first to file descriptor 3 to restore it later
  exec 3>&1
  exec 1>&2

  echo "$(tstamp)Splitting audio files ${AUDIOFILES[@]}"
  python $SPLIT_AUDIO --max-duration $MAXIMUM_TRANSCRIPTION_LENGTH \
    ${AUDIOFILES[@]}

  AUDIOFILES_TRANSCRIBE=()
  for ID in $IDS; do
    AUDIOFILES_TRANSCRIBE+=($WAVE_TMP_DIR/*-$ID.wav.???)
  done

  while [[ ${#AUDIOFILES_TRANSCRIBE[@]} -gt 0 ]];
  do
    echo "$(tstamp)Converting audio files ${AUDIOFILES_TRANSCRIBE[@]} to MIDI..."
    if onsets_frames_transcription_transcribe \
      --model_dir="${MODEL_DIR}" --log="ERROR" ${AUDIOFILES_TRANSCRIBE[@]}; then
      # All files succeeded, no files left to process
      AUDIOFILES_TRANSCRIBE=()
    else
      # Remove all files from queue that have been successfully transcribed
      for AUDIOFILE in ${AUDIOFILES_TRANSCRIBE[@]}; do
        if [[ -e "$AUDIOFILE.midi" ]]; then
          AUDIOFILES_TRANSCRIBE=( ${AUDIOFILES_TRANSCRIBE[@]/$AUDIOFILE/} )
        fi
      done
      FIRSTFAILED=${AUDIOFILES_TRANSCRIBE[0]}
      # Remove extensions
      FAILEDSTEM=${FIRSTFAILED%.wav.???}
      # Remove extensions
      # Get ID (11 characters long from the right)
      FAILEDID=${FAILEDSTEM:(-11):11}
      IDS=( ${IDS[@]/$FAILEDID/} )
      # Remove extensions
      # Remove all occurrences of ID in queue of audios waiting to be transcribed,
      # if one part could not be transcribed, the whole ID is corrupted.
      AUDIOFILES_TRANSCRIBE=( ${AUDIOFILES_TRANSCRIBE[@]/*$FAILEDID*/} )
      # echo "$(tstamp)Transcription of video $FAILEDID failed" 1>&3
      echo "$(tstamp)Transcription of video $FAILEDID failed"
      flock $LOG_FILE_IDS \
        sed -i -e "/${FAILEDID}.*--\$/ s/\$/ transcription failed/" $LOG_FILE_IDS
    fi
  done

  for ID in ${IDS[@]}; do
    MIDIFILES=($WAVE_TMP_DIR/*-$ID.wav.*.midi)
    echo "$(tstamp)Merging MIDI files ${MIDIFILES[@]}..."
    python $MERGE_MIDIS ${MIDIFILES[@]}
    mv $WAVE_TMP_DIR/*-$ID.wav.midi $MIDI_DIR
    rm ${MIDIFILES[@]}
  done

  if [ $KEEP_WAVES -eq 1 ]; then
    cp ${AUDIOFILES[@]} $WAVE_DIR
  fi

  for ID in ${IDS[@]}; do
    rm $WAVE_TMP_DIR/*-$ID.wav*
  done

  # Echo again to stdout
  exec 1>&3

  for ID in ${IDS[@]}; do
    echo "$(tstamp)Successfully transcribed video $ID"
    flock $LOG_FILE_IDS \
      sed -i -e "/${ID}.*--\$/ s/\$/ transcription OK/" $LOG_FILE_IDS
  done
}

export -f transcribe_audio

clean_up() {
  rm -r "$WAVE_TMP_DIR"
  rm $QUEUEFILE

  if [ -n "$MAGENTA_ENV_NAME" ]; then
    conda deactivate
  fi
}

# Calculate maximum length of a slice that should be transcribed in seconds,
# but only if it has not already been set.
#
# Memory usage scales in good approximation linearly with slice length,
# about 6 MB / second or 0.17 seconds / MB

# Free memory in kilobytes
FREEMEM=$(awk '/MemFree/ { print $2 }' /proc/meminfo)

if [ -z ${MAXIMUM_TRANSCRIPTION_LENGTH+unset} ]; then
  echo "Free memory: $FREEMEM kilobytes"
  # Conservative assumption: Use 80% of free memory for all processes
  MAXIMUM_TRANSCRIPTION_LENGTH=$(($FREEMEM * 17 / 125000 / $JOBSTRANSCRIBE))
  echo "Maximum transcription length (80% of free memory for"  \
    "$JOBSTRANSCRIBE jobs): $MAXIMUM_TRANSCRIPTION_LENGTH seconds"
fi

export MAXIMUM_TRANSCRIPTION_LENGTH

export VIDEOCOUNT=$(cat $URLFILE | wc -l)
echo "Start downloading $VIDEOCOUNT videos with $JOBSDOWNLOAD jobs..."
touch $QUEUEFILE

# Ignore exit status of `parallel`, we need to terminate
# the queue even if some download jobs failed
{ parallel -a $URLFILE --jobs $JOBSDOWNLOAD --retries 2 download_video {} {#} \
    2>$LOG_FILE_DOWNLOAD || true; echo "EOF" >> $QUEUEFILE; } &

echo "Start transcribing videos..."
# Disable pipefail mode here, because otherwise the script will
# abort immediately after `tail` terminating with exit code
# 141 (SIGPIPE), which happens when `parallel` stops reading
# from the pipe (after encountering 'EOF')
set +o pipefail
tail -f $QUEUEFILE | parallel -E EOF -N$BATCHSIZE --no-run-if-empty \
  --jobs $JOBSTRANSCRIBE --linebuffer \
  transcribe_audio {#} {} 2>$LOG_FILE_TRANSCRIPT

echo "Finished. Cleaning up..."
clean_up
